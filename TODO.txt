Restore previous view when rotate.

1. Add reminder
 the event details should be added to the calendar with the reminder.

2. How to change start week by Monday instead of Sunday?
private void setUpHeader(Calendar today, SimpleDateFormat weekDayFormatter, Locale locale) {
int daysPerWeek = 7;
String[] dayLabels = new String[daysPerWeek];
Calendar cal = Calendar.getInstance(CalendarManager.getInstance(getContext()).getLocale());
cal.setTime(today.getTime());
int firstDayOfWeek = cal.getFirstDayOfWeek();

      for (int count = 0; count < 7; count++) {
        cal.set(Calendar.DAY_OF_WEEK, firstDayOfWeek + count);
        if (locale.getLanguage().equals("en")) {
            dayLabels[count] = weekDayFormatter.format(cal.getTime()).toUpperCase(locale);
        } else {
            dayLabels[count] = weekDayFormatter.format(cal.getTime());
        }
    }
for (int i = 0; i < mDayNamesHeader.getChildCount(); i++) {
TextView txtDay = (TextView) mDayNamesHeader.getChildAt(i);
txtDay.setText(dayLabels[i]);
}
}