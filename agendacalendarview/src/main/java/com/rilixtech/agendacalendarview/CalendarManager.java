package com.rilixtech.agendacalendarview;

import android.support.annotation.NonNull;
import com.rilixtech.agendacalendarview.models.BaseCalendarEvent;
import com.rilixtech.agendacalendarview.models.CalendarEvent;
import com.rilixtech.agendacalendarview.models.DayItem;
import com.rilixtech.agendacalendarview.models.IDayItem;
import com.rilixtech.agendacalendarview.models.IWeekItem;
import com.rilixtech.agendacalendarview.models.WeekItem;
import com.rilixtech.agendacalendarview.utils.DateHelper;
import android.content.Context;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * This class manages information about the calendar. (Events, weather info...)
 * Holds reference to the days list of the calendar.
 * As the app is using several views, we want to keep everything in one place.
 */
public class CalendarManager {
  private static final String TAG = CalendarManager.class.getSimpleName();

  private static CalendarManager mInstance;
  private Context mContext;
  private Locale mLocale;
  private Calendar mCalendarToday = null;
  private SimpleDateFormat mWeekdayFormatter;
  private SimpleDateFormat mMonthHalfNameFormat;
  private List<Integer> weekends = null;
  private int mWeekendsColor;
  private Calendar mTempCalendar;
  private int mFirstDayOfWeek;
  private int mEmptyEventVisibility;

  /**
   * List of days used by the calendar
   */
  private List<IDayItem> mDays;
  /**
   * List of weeks used by the calendar
   */
  private List<IWeekItem> mWeeks;
  /**
   * List of events instances
   */
  private List<CalendarEvent> mEvents;

  private CalendarManager(Context context) {
    this.mContext = context;
    // use default locale when locale is not set
    this.mLocale = Locale.getDefault();
    mDays = new ArrayList<>();
    mWeeks = new ArrayList<>();
    mEvents = new ArrayList<>();
  }

  static CalendarManager initInstance(Context context) {
    if (mInstance == null) mInstance = new CalendarManager(context);
    return mInstance;
  }

  public static CalendarManager getInstance() {
    if (mInstance == null) throw new RuntimeException("call CalendarManager.initInstance first!");
    return mInstance;
  }

  public Locale getLocale() {
    return mLocale;
  }

  public Context getContext() {
    return mContext;
  }

  public Calendar getToday() {
    if (mCalendarToday == null) {
      mCalendarToday = (mLocale == null) ? Calendar.getInstance() : Calendar.getInstance(mLocale);
    }
    return mCalendarToday;
  }

  public void setTodayCalendar(Calendar today) {
    this.mCalendarToday = today;
  }

  public List<IWeekItem> getWeeks() {
    return mWeeks;
  }

  public List<CalendarEvent> getEvents() {
    return mEvents;
  }

  public List<IDayItem> getDays() {
    return mDays;
  }

  public SimpleDateFormat getWeekdayFormatter() {
    return mWeekdayFormatter;
  }

  public SimpleDateFormat getMonthHalfNameFormat() {
    return mMonthHalfNameFormat;
  }

  public List<Integer> getWeekends() {
    return weekends;
  }

  public void setWeekends(List<Integer> weekends) {
    this.weekends = weekends;
  }

  public int getWeekendsColor() {
    return mWeekendsColor;
  }

  public void setWeekendsColor(int weekendsColor) {
    this.mWeekendsColor = weekendsColor;
  }

  public void setEmptyEventVisibility(int visibility) {
    mEmptyEventVisibility = visibility;
  }

  public int getEmptyEventVisibility() {
    return mEmptyEventVisibility;
  }

  void buildCalendar(@NonNull Calendar minDate, @NonNull Calendar maxDate, @NonNull Locale locale) {
    setLocale(locale);
    mDays.clear();
    mWeeks.clear();
    mEvents.clear();

    // maxDate is exclusive, here we bump back to the previous day, as maxDate if December 1st, 2020,
    // we don't include that month in our list
    maxDate.add(Calendar.MINUTE, -1);

    // Now we iterate between minDate and maxDate so we init our list of weeks
    int maxMonth = maxDate.get(Calendar.MONTH);
    int maxYear = maxDate.get(Calendar.YEAR);
    int currentMonth = minDate.get(Calendar.MONTH);
    int currentYear = minDate.get(Calendar.YEAR);

    while ((currentMonth <= maxMonth // Up to, including the month.
        || currentYear < maxYear) // Up to the year.
        && currentYear < maxYear + 1) { // But not > next yr.

      IWeekItem weekItem = generateWeek(minDate, currentYear, currentMonth);
      mWeeks.add(weekItem);

      //Log.d(TAG, String.format("Adding week: %s", weekItem));
      minDate.add(Calendar.WEEK_OF_YEAR, 1);
      currentMonth = minDate.get(Calendar.MONTH);
      currentYear = minDate.get(Calendar.YEAR);
    }
  }

  /**
   * Generate week item by month and year
   *
   * @param calendar calendar of the week
   * @param year year of the week
   * @param month month of the week
   * @return week item
   */
  private IWeekItem generateWeek(Calendar calendar, int year, int month) {
    Date date = calendar.getTime();
    int weekOfYear = calendar.get(Calendar.WEEK_OF_YEAR);
    IWeekItem weekItem = new WeekItem();
    weekItem.setWeekInYear(weekOfYear);
    weekItem.setYear(year);
    weekItem.setDate(date);
    weekItem.setMonth(month);
    weekItem.setLabel(mMonthHalfNameFormat.format(date));
    List<IDayItem> dayItems = getDayCells(calendar); // gather days for the built week
    weekItem.setDayItems(dayItems);
    return weekItem;
  }

  /**
   * Init events without checking if there are events
   * This only be used when creating the AgendaCalendarView for the first time.
   */
  void initEvents(List<CalendarEvent> events) {
    for (int i = 0; i < getWeeks().size(); i++) {
      IWeekItem weekItem = getWeeks().get(i);

      List<IDayItem> dayItems = weekItem.getDayItems();
      for (int weekItemIdx = 0; weekItemIdx < dayItems.size(); weekItemIdx++) {
        DayItem dayItem = (DayItem) dayItems.get(weekItemIdx);
        boolean isEventForAnotherDay = true;
        for (int eventIdx = 0; eventIdx < events.size(); eventIdx++) {
          CalendarEvent event = events.get(eventIdx);
          if (DateHelper.isBetweenInclusive(dayItem.getDate(), event.getStartTime(), event.getEndTime())) {
            CalendarEvent dayEvent = createEvent(event, dayItem, weekItem);
            mEvents.add(dayEvent);
            isEventForAnotherDay = false;
          }
        }

        if (isEventForAnotherDay) setEmptyEvent(weekItem, dayItem);
      }
    }
  }

  void addEvent(CalendarEvent newEvent) {
    for (int i = 0; i < getWeeks().size(); i++) {
      IWeekItem weekItem = getWeeks().get(i);
      List<IDayItem> dayItems = weekItem.getDayItems();
      for (int weekItemIdx = 0; weekItemIdx < dayItems.size(); weekItemIdx++) {
        DayItem dayItem = (DayItem) dayItems.get(weekItemIdx);
        if (DateHelper.isBetweenInclusive(dayItem.getDate(), newEvent.getStartTime(),
            newEvent.getEndTime())) {
          CalendarEvent dayEvent = createEvent(newEvent, dayItem, weekItem);
          saveEvent(mEvents, dayEvent);
        }
      }
    }
  }

  private CalendarEvent createEvent(CalendarEvent event, IDayItem dayItem, IWeekItem weekItem) {
    CalendarEvent dayEvent = (CalendarEvent) event.copy();
    Calendar dayInstance = (Calendar) mTempCalendar.clone();
    dayInstance.setTime(dayItem.getDate());

    // check if day is a weekend
    int dayOfWeek = dayInstance.get(Calendar.DAY_OF_WEEK);
    if (weekends != null) dayItem.setWeekend(weekends.contains(dayOfWeek));

    dayItem.setEventTotal(dayItem.getEventTotal() + 1);
    dayItem.setColor(event.getCalendarDayColor());
    dayEvent.instanceDay(dayInstance);
    dayEvent.dayReference(dayItem);
    dayEvent.weekReference(weekItem);
    return dayEvent;
  }

  private CalendarEvent changeAsEmptyEvent(CalendarEvent event) {
    event.startTime(null);
    event.getDayReference().setEventTotal(0);
    event.getDayReference().setColor(0);
    event.location("");
    event.color(0);
    event.title(mContext.getString(R.string.agenda_event_no_events));
    event.placeholder(true);
    event.visibility(mEmptyEventVisibility);
    return event;
  }

  private CalendarEvent createEmptyEvent(IDayItem dayItem, IWeekItem weekItem) {
    Calendar dayInstance = (Calendar) mTempCalendar.clone();
    dayInstance.setTime(dayItem.getDate());

    // check if day is a weekend
    int dayOfWeek = dayInstance.get(Calendar.DAY_OF_WEEK);
    if (weekends != null) dayItem.setWeekend(weekends.contains(dayOfWeek));

    CalendarEvent event = new BaseCalendarEvent();
    event.instanceDay(dayInstance);
    event.startTime(null);
    event.dayReference(dayItem);
    event.weekReference(weekItem);
    event.location("");
    event.title(mContext.getString(R.string.agenda_event_no_events));
    event.placeholder(true);
    event.visibility(mEmptyEventVisibility);
    return event;
  }

  /**
   * Remove an Event from Calendar
   *
   * @param eventToRemove CalendarEvent to be removed
   * @return position of removed event.
   * -2 if event is an empty event.
   * -1 if event is not found.
   */
  int deleteEvent(CalendarEvent eventToRemove) {
    // ignore empty event
    if (eventToRemove.getStartTime() == null) return -2;

    int position = mEvents.indexOf(eventToRemove);
    if (position == -1) return position;

    IDayItem dayItem = eventToRemove.getDayReference();
    IWeekItem weekItem = eventToRemove.getWeekReference();
    boolean isTheOnlyEventForTheDate = true;
    for (CalendarEvent event : mEvents) {
      // ignore empty event
      if (event.getStartTime() == null) continue;

      // Don't count itself
      if (event.equals(eventToRemove)) continue;

      if (event.getWeekReference().equals(weekItem) && event.getDayReference().equals(dayItem)) {
        isTheOnlyEventForTheDate = false;
        break;
      }
    }

    dayItem.setEventTotal(dayItem.getEventTotal() - 1);

    if (isTheOnlyEventForTheDate) {
      changeAsEmptyEvent(eventToRemove);
    } else {
      mEvents.remove(position);
    }
    return position;
  }

  private int getPositionOfDate(CalendarEvent event, CalendarEvent againstEvent, int againstPosition) {

    // check position of event
    DateHelper.DatePosition datePosition = DateHelper.getPositionOfDate(event.getStartTime(),
        againstEvent.getStartTime());

    int position = -1;
    switch (datePosition) {
      case BEFORE:
        if (againstPosition == 0) {
          position = 0;
        } else {
          position = againstPosition - 1;
        }
        break;
      case AFTER:
        position = againstPosition + 1;
        break;
      case SAME:
        position = againstPosition;
        break;
      case UNKNOWN:
        // this shouldn't happens!
    }

    return position;
  }

  private void saveEvent(List<CalendarEvent> events, CalendarEvent newEvent) {
    for (int i = 0; i < events.size(); i++) {
      CalendarEvent event = events.get(i);

      if (event.getWeekReference().equals(newEvent.getWeekReference())
          && event.getDayReference().equals(newEvent.getDayReference())) {
        if (event.getStartTime() == null) {
          events.set(i, newEvent);
        } else {
          int position = getPositionOfDate(newEvent, event, i);
          events.add(position, newEvent);
        }
        // stop searching when founded.
        break;
      }
    }
  }

  private void setEmptyEvent(IWeekItem weekItem, IDayItem dayItem) {
    CalendarEvent event = createEmptyEvent(dayItem, weekItem);
    mEvents.add(mEvents.size(), event);
  }

  private List<IDayItem> getDayCells(Calendar startCal) {
    if (mTempCalendar == null) mTempCalendar = Calendar.getInstance(mLocale);
    mTempCalendar.setTime(startCal.getTime());
    List<IDayItem> dayItems = new ArrayList<>();

    int dayOfWeek = mTempCalendar.get(Calendar.DAY_OF_WEEK);
    int firstDayOfWeek;
    if (mFirstDayOfWeek == -1) {
      firstDayOfWeek = mTempCalendar.getFirstDayOfWeek();
    } else {
      firstDayOfWeek = mFirstDayOfWeek;
    }

    int offset = firstDayOfWeek - dayOfWeek;
    if (offset > 0) {
      offset -= 7;
    }

    mTempCalendar.add(Calendar.DATE, offset);

    //Log.d(TAG, String.format("Building row week starting at %s", mTempCalendar.getTime()));
    for (int i = 0; i < 7; i++) {
      IDayItem dayItem = new DayItem();
      dayItem.buildDayItemFromCal(mTempCalendar);
      dayItems.add(dayItem);
      mTempCalendar.add(Calendar.DATE, 1);
    }

    mDays.addAll(dayItems);
    return dayItems;
  }

  private void setLocale(Locale locale) {
    mLocale = locale;
    mCalendarToday = Calendar.getInstance(locale);
    mWeekdayFormatter = new SimpleDateFormat(mContext.getString(R.string.day_name_format), locale);
    String monthHalfNameFormat = mContext.getString(R.string.month_half_name_format);
    mMonthHalfNameFormat = new SimpleDateFormat(monthHalfNameFormat, locale);
  }

  public Calendar getTempCalendar() {
    if (mTempCalendar == null) mTempCalendar = Calendar.getInstance(mLocale);
    return mTempCalendar;
  }

  public void setFirstDayOfWeek(int firstDayOfWeek) {
    this.mFirstDayOfWeek = firstDayOfWeek;
  }

  /**
   * Get events of the specific month
   *
   * @param month month of the event. Remember that the month is start from 0
   * @param year year of the event.
   * @return List of the events.
   */
  List<CalendarEvent> getEvents(int month, int year) {
    if (month < 0 || month > 12) return Collections.emptyList();

    // create start of the month
    Calendar startCal = Calendar.getInstance();
    startCal.set(Calendar.DAY_OF_MONTH, 1);
    startCal.set(Calendar.MONTH, month);
    startCal.set(Calendar.YEAR, year);
    startCal.set(Calendar.HOUR_OF_DAY, 0);
    startCal.set(Calendar.MINUTE, 0);
    startCal.set(Calendar.SECOND, 0);
    startCal.set(Calendar.MILLISECOND, 0);

    // create end of the month
    Calendar endCal = Calendar.getInstance();
    endCal.set(Calendar.DAY_OF_MONTH, startCal.getActualMaximum(Calendar.DAY_OF_MONTH));
    endCal.set(Calendar.MONTH, month);
    endCal.set(Calendar.YEAR, year);
    endCal.set(Calendar.HOUR_OF_DAY, 23);
    endCal.set(Calendar.MINUTE, 59);
    endCal.set(Calendar.SECOND, 59);
    endCal.set(Calendar.MILLISECOND, 999);

    List<CalendarEvent> events = new ArrayList<>();

    for (int i = 0; i < mEvents.size(); i++) {
      CalendarEvent event = mEvents.get(i);

      if (event.getStartTime() == null) continue;

      if (DateHelper.isBetweenInclusive(event.getDayReference().getDate(), startCal, endCal)) {
        CalendarEvent ev = (CalendarEvent) event.copy();
        events.add(ev);
      }
    }

    return events;
  }

  List<CalendarEvent> getEvents(int day, int month, int year, boolean ignoreEmptyEvent) {
    if (day > 31) return Collections.emptyList();
    if (day < 1) return Collections.emptyList();
    if (month < 0) return Collections.emptyList();
    if (month > 12) return Collections.emptyList();

    // Create calendar based on the date
    Calendar startCal = Calendar.getInstance();
    startCal.set(Calendar.DAY_OF_MONTH, day);
    startCal.set(Calendar.MONTH, month);
    startCal.set(Calendar.YEAR, year);
    startCal.set(Calendar.HOUR_OF_DAY, 0);
    startCal.set(Calendar.MINUTE, 0);
    startCal.set(Calendar.SECOND, 0);
    startCal.set(Calendar.MILLISECOND, 0);

    Calendar endCal = Calendar.getInstance();
    endCal.set(Calendar.DAY_OF_MONTH, day);
    endCal.set(Calendar.MONTH, month);
    endCal.set(Calendar.YEAR, year);
    endCal.set(Calendar.HOUR_OF_DAY, 23);
    endCal.set(Calendar.MINUTE, 59);
    endCal.set(Calendar.SECOND, 59);
    endCal.set(Calendar.MILLISECOND, 999);

    List<CalendarEvent> events = new ArrayList<>();

    for (int i = 0; i < mEvents.size(); i++) {
      CalendarEvent event = mEvents.get(i);

      // if ignore empty event is set, ignore the empty event from list.
      if (ignoreEmptyEvent && event.getStartTime() == null) continue;

      if (DateHelper.isBetweenInclusive(event.getDayReference().getDate(), startCal, endCal)) {
        CalendarEvent ev = (CalendarEvent) event.copy();
        events.add(ev);
      }
    }

    return events;
  }
}
