package com.rilixtech.agendacalendarview.render;

import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.rilixtech.agendacalendarview.R;
import com.rilixtech.agendacalendarview.agenda.AgendaEventView;
import com.rilixtech.agendacalendarview.models.BaseCalendarEvent;

/**
 * Class helping to inflate our default layout in the AgendaAdapter
 */
public class DefaultEventRenderer extends AbstractEventRenderer<BaseCalendarEvent> {

  @Override
  public View render(ViewGroup parent, View convertView, @NonNull BaseCalendarEvent event) {
    ViewHolder viewHolder;
    if (convertView == null) {
      viewHolder = new ViewHolder();
      LayoutInflater inflater = LayoutInflater.from(parent.getContext());
      convertView = inflater.inflate(R.layout.view_agenda_event, parent, false);
      AgendaEventView eventView = (AgendaEventView) convertView;
      viewHolder.llyDescContainerLeft = (LinearLayout) eventView.getChildAt(0);
      viewHolder.llyDescContainer = (LinearLayout) eventView.getChildAt(1);
      viewHolder.tvTitle = (TextView) viewHolder.llyDescContainer.getChildAt(0);
      viewHolder.llyLocContainer = (LinearLayout) viewHolder.llyDescContainer.getChildAt(1);
      viewHolder.imvLocation = (ImageView) viewHolder.llyLocContainer.getChildAt(0);
      viewHolder.tvLocation = (TextView) viewHolder.llyLocContainer.getChildAt(1);
      convertView.setTag(viewHolder);
    } else {
      viewHolder = (ViewHolder) convertView.getTag();
    }

    Resources resources = convertView.getResources();
    viewHolder.llyDescContainer.setVisibility(View.VISIBLE);

    if (event.getLocation().length() > 0) {
      viewHolder.llyLocContainer.setVisibility(View.VISIBLE);
      viewHolder.tvLocation.setText(event.getLocation());
      viewHolder.tvLocation.setText(event.getLocation());
      int color;
      if (event.getLocationTextColor() == 0) {
        color = resources.getColor(R.color.theme_text_icons);
      } else {
        color = event.getLocationTextColor();
      }
      viewHolder.tvLocation.setTextColor(color);
      viewHolder.imvLocation.setBackgroundResource(R.drawable.img_location);
    } else {
      viewHolder.llyLocContainer.setVisibility(View.GONE);
    }

    viewHolder.tvTitle.setText(event.getTitle());
    if (event.getStartTime() == null) {
      viewHolder.tvTitle.setTextColor(resources.getColor(R.color.blue_selected));
    } else {
      int color;
      if (event.getTitleTextColor() == 0) {
        color = resources.getColor(R.color.theme_text_icons);
      } else {
        color = event.getTitleTextColor();
      }
      viewHolder.tvTitle.setTextColor(color);
    }
    viewHolder.llyDescContainerLeft.setBackgroundColor(event.getColor());

    convertView.setVisibility(event.getVisibility());
    return convertView;
  }

  @Override public AbstractEventRenderer<?> copy() {
    return null;
  }

  private static class ViewHolder {
    TextView tvTitle;
    TextView tvLocation;
    LinearLayout llyDescContainer;
    LinearLayout llyLocContainer;
    LinearLayout llyDescContainerLeft;
    ImageView imvLocation;
  }
}
