package com.rilixtech.agendacalendarview;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.RotateAnimation;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import com.rilixtech.agendacalendarview.agenda.AgendaAdapter;
import com.rilixtech.agendacalendarview.agenda.AgendaView;
import com.rilixtech.agendacalendarview.calendar.CalendarView;
import com.rilixtech.agendacalendarview.calendar.weekslist.WeeksDayClickListener;
import com.rilixtech.agendacalendarview.models.CalendarEvent;
import com.rilixtech.agendacalendarview.models.IDayItem;
import com.rilixtech.agendacalendarview.render.AbstractEventRenderer;
import com.rilixtech.agendacalendarview.utils.ListViewScrollTracker;
import com.rilixtech.agendacalendarview.widgets.FloatingActionButton;
import com.rilixtech.stickylistheaders.StickyListHeadersListView;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * View holding the agenda and calendar view together.
 */
public class AgendaCalendarView extends FrameLayout
    implements StickyListHeadersListView.OnStickyHeaderChangedListener, WeeksDayClickListener,
    AgendaView.OnAgendaViewDayItemClickListener {

  private static final String TAG = AgendaCalendarView.class.getSimpleName();

  private CalendarView mCalendarView;
  private AgendaView mAgendaView;
  private FloatingActionButton mFabDirection;

  private int mAgendaCurrentDayTextColor;
  private int mCalendarHeaderColor;
  private int mCalendarBackgroundColor;
  private int mCalendarDayTextColor;
  private int mCalendarPastDayTextColor;
  private int mCalendarCurrentDayColor;
  private int mFabColor;
  private int mWeekendColor;
  private int mEventEmptyVisibility;

  private AgendaCalendarViewListener agendaCalendarViewListener;
  private AgendaCalendarViewFetchListener mAgendaCalendarViewFetchListener;

  private ListViewScrollTracker mAlvScrollTracker;

  private CalendarManager mCalendarManager;

  public interface AgendaCalendarViewListener {
    void onDaySelected(IDayItem dayItem);
    void onEventClicked(View view, CalendarEvent event);
    void onScrollToDate(Calendar calendar);
    void onEventLongClicked(View view, CalendarEvent event);
  }

  void setAgendaCalendarViewFetchListener(AgendaCalendarViewFetchListener listener) {
    mAgendaCalendarViewFetchListener = listener;
  }

  private AbsListView.OnScrollListener mAgendaScrollListener = new AbsListView.OnScrollListener() {
    private int mCurrentAngle;
    private int mMaxAngle = 85;

    @Override public void onScrollStateChanged(AbsListView view, int scrollState) {

    }

    @Override public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount,
        int totalItemCount) {
      int scrollY = mAlvScrollTracker.calculateScrollY(firstVisibleItem, visibleItemCount);
      if (scrollY != 0) mFabDirection.show();

      Log.d(TAG, String.format("Agenda listView scrollY: %d", scrollY));
      int toAngle = scrollY / 100;
      if (toAngle > mMaxAngle) {
        toAngle = mMaxAngle;
      } else if (toAngle < -mMaxAngle) {
        toAngle = -mMaxAngle;
      }
      RotateAnimation rotate =
          new RotateAnimation(mCurrentAngle, toAngle, mFabDirection.getWidth() / 2,
              mFabDirection.getHeight() / 2);
      rotate.setFillAfter(true);
      mCurrentAngle = toAngle;
      mFabDirection.startAnimation(rotate);
    }
  };

  public AgendaCalendarView(Context context) {
    super(context);
  }

  public AgendaCalendarView(Context context, AttributeSet attrs) {
    super(context, attrs);

    TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.AgendaCalendarView, 0, 0);

    int defAgendaCurrentDayTextColor = getResources().getColor(R.color.theme_primary);
    //int defBackgroundColor = defAgendaCurrentDayTextColor;
    int defFabColor = getResources().getColor(R.color.theme_accent);
    int defHeaderColor = getResources().getColor(R.color.theme_light_primary);
    int defDayTextColor = getResources().getColor(R.color.theme_text_icons);
    int defCalCurrentDayColor = getResources().getColor(R.color.calendar_text_current_day);
    int defPastDayTextColor = getResources().getColor(R.color.theme_light_primary);

    int weekendColor = getResources().getColor(R.color.calendar_day_weekend);

    mAgendaCurrentDayTextColor =
        a.getColor(R.styleable.AgendaCalendarView_acv_agendaCurrentDayTextColor,
            defAgendaCurrentDayTextColor);
    mCalendarHeaderColor =
        a.getColor(R.styleable.AgendaCalendarView_acv_calendarHeaderColor, defHeaderColor);
    mCalendarBackgroundColor =
        a.getColor(R.styleable.AgendaCalendarView_acv_calendarColor, defAgendaCurrentDayTextColor);
    mCalendarDayTextColor =
        a.getColor(R.styleable.AgendaCalendarView_acv_calendarDayTextColor, defDayTextColor);
    mCalendarCurrentDayColor =
        a.getColor(R.styleable.AgendaCalendarView_acv_calendarCurrentDayTextColor,
            defCalCurrentDayColor);
    mCalendarPastDayTextColor =
        a.getColor(R.styleable.AgendaCalendarView_acv_calendarPastDayTextColor,
            defPastDayTextColor);
    mFabColor = a.getColor(R.styleable.AgendaCalendarView_acv_fabColor, defFabColor);
    mWeekendColor = a.getColor(R.styleable.AgendaCalendarView_acv_weekendColor, weekendColor);

    // default is visible for empty event.
    mEventEmptyVisibility =
        a.getInt(R.styleable.AgendaCalendarView_acv_emptyEventVisibility, View.VISIBLE);

    setAlpha(0f);
    a.recycle();

    inflateCalendarView();
  }

  private void inflateCalendarView() {
    LayoutInflater inflater = LayoutInflater.from(getContext());
    inflater.inflate(R.layout.view_agendacalendar, this, true);

    mCalendarView = (CalendarView) getChildAt(0);
    mAgendaView = (AgendaView) getChildAt(1);
    mFabDirection = (FloatingActionButton) getChildAt(2);

    ColorStateList csl = new ColorStateList(new int[][] { new int[0] }, new int[] { mFabColor });
    mFabDirection.setBackgroundTintList(csl);

    mCalendarView.findViewById(R.id.cal_day_names).setBackgroundColor(mCalendarHeaderColor);
    mCalendarView.findViewById(R.id.list_week).setBackgroundColor(mCalendarBackgroundColor);

    mAgendaView.setOnAgendaViewDayItemClickListener(this);
    mAgendaView.getAgendaListView()
        .setOnItemClickListener(new AdapterView.OnItemClickListener() {
          @Override
          public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            List<CalendarEvent> events = mCalendarManager.getEvents();
            agendaCalendarViewListener.onEventClicked(view, events.get(position));
          }
        });

    mAgendaView.getAgendaListView().setOnItemLongClickListener(
        new AdapterView.OnItemLongClickListener() {
          @Override
          public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
            List<CalendarEvent> events = mCalendarManager.getEvents();
            agendaCalendarViewListener.onEventLongClicked(view, events.get(position));
            return false;
          }
        });
  }

  private void animateView() {
    ObjectAnimator alphaAnimation =
        ObjectAnimator.ofFloat(this, "alpha", getAlpha(), 1f).setDuration(500);

    alphaAnimation.addListener(new Animator.AnimatorListener() {
      @Override public void onAnimationStart(Animator animation) {

      }

      @Override public void onAnimationEnd(Animator animation) {
        final long fabAnimationDelay = 500;
        // Just after setting the alpha from this view to 1, we hide the fab.
        // It will reappear as soon as the user is scrolling the Agenda view.
        new Handler().postDelayed(new Runnable() {
          @Override public void run() {
            mFabDirection.hide();
            mAlvScrollTracker = new ListViewScrollTracker(mAgendaView.getAgendaListView());
            mAgendaView.getAgendaListView().setOnScrollListener(mAgendaScrollListener);
            mFabDirection.setOnClickListener(new OnClickListener() {
              @Override public void onClick(View v) {
                mAgendaView.translateList(0);
                mAgendaView.getAgendaListView().scrollToCurrentDate(mCalendarManager.getToday());
                new Handler().postDelayed(new Runnable() {
                  @Override public void run() {
                    mFabDirection.hide();
                  }
                }, fabAnimationDelay);
              }
            });
          }
        }, fabAnimationDelay);
      }

      @Override public void onAnimationCancel(Animator animation) {

      }

      @Override public void onAnimationRepeat(Animator animation) {

      }
    });
    alphaAnimation.start();

    mAgendaCalendarViewFetchListener.onAgendaCalendarViewEventFetched();
  }

  @Override public void onStickyHeaderChanged(StickyListHeadersListView stickyListHeadersListView,
      View header, int position, long headerId) {
    Log.d(TAG, String.format("onStickyHeaderChanged, position = %d, headerId = %d", position, headerId));

    if (mCalendarManager.getEvents().size() > 0) {
      CalendarEvent event = mCalendarManager.getEvents().get(position);
      scrollToEvent(event);
    }
  }

  /**
   * Scroll to specific event.
   * @param event Scroll to th event.
   * @return true if scroll can be done, false if not.
   */
  public boolean scrollToEvent(CalendarEvent event) {
    if (event == null) return false;
    mCalendarView.scrollToDate(event);
    agendaCalendarViewListener.onScrollToDate(event.getInstanceDay());
    return true;
  }

  /**
   * Scroll to specific date by day, month, and year.
   * To scroll to 1 November 2018, call with scrollToDate(1, 10, 2018)
   * the month is real month - 1 because this is the specification of a Calendar which
   * is used internally by the View.
   * @param day day number in a month
   * @param month month of date
   * @param year year of date
   * @return true if scroll can be done, false if not.
   */
  public boolean scrollToDate(int day, int month, int year) {
    if(day > 31) return false;
    if(day < 1) return false;
    if(month < 0) return false;
    if(month > 12) return false;

    // first, get all the events of the date
    List<CalendarEvent> events = mCalendarManager.getEvents(day, month, year, false);
    if(events.size() == 0) return false;
    mAgendaView.getAgendaListView().scrollToCurrentDate(events.get(0).getInstanceDay());
    return true;
  }

  private void initCalendarManager(Calendar minDate, Calendar maxDate, List<CalendarEvent> events,
      Locale locale, List<Integer> weekends, int weekendColor, int firstDayOfWeek,
      int emptyEventVisibility) {
    mCalendarManager = CalendarManager.initInstance(getContext());
    mCalendarManager.setWeekends(weekends);
    mCalendarManager.setWeekendsColor(weekendColor);
    mCalendarManager.setFirstDayOfWeek(firstDayOfWeek);
    mCalendarManager.setEmptyEventVisibility(emptyEventVisibility);
    if (locale == null) locale = Locale.getDefault();
    mCalendarManager.buildCalendar(minDate, maxDate, locale);
    mCalendarManager.initEvents(events);
  }

  private Calendar minimumDate = null;
  private Calendar maximumDate = null;
  private Locale locale = null;
  private List<CalendarEvent> events = null;
  private AbstractEventRenderer<?> mAbstractEventRenderer = null;
  private List<Integer> weekends = null;
  private int firstDayOfWeek = -1;

  public AgendaCalendarView setMinimumDate(@NonNull Calendar minimumDate) {
    this.minimumDate = minimumDate;
    return this;
  }

  public AgendaCalendarView setMaximumDate(@NonNull Calendar maximumDate) {
    this.maximumDate = maximumDate;
    return this;
  }

  public AgendaCalendarView setCalendarEvents(@NonNull List<CalendarEvent> events) {
    this.events = events;
    return this;
  }

  public AgendaCalendarView setLocale(@Nullable Locale locale) {
    this.locale = locale;
    return this;
  }

  public AgendaCalendarView setAgendaCalendarViewListener(AgendaCalendarViewListener listener) {
    agendaCalendarViewListener = listener;
    return this;
  }

  public AgendaCalendarView setEventRender(@Nullable AbstractEventRenderer<?> renderer) {
    mAbstractEventRenderer = renderer;
    return this;
  }

  public AgendaCalendarView setWeekends(@Nullable List<Integer> weekends) {
    this.weekends = weekends;
    return this;
  }

  public AgendaCalendarView setWeekendsColor(int weekendsColor) {
    this.mWeekendColor = weekendsColor;
    return this;
  }

  // set Calendar.firstDayOfWeek
  public AgendaCalendarView setFirstDayOfWeek(int firstDayOfWeek) {
    this.firstDayOfWeek = firstDayOfWeek;
    return this;
  }

  public void build() {
    if (minimumDate == null) throw new RuntimeException("Please set minimumDate");
    if (maximumDate == null) throw new RuntimeException("Please set maximumDate");
    if (events == null) throw new RuntimeException("Please set events");
    if (agendaCalendarViewListener == null) throw new RuntimeException("Please set Listener");

    init(minimumDate, maximumDate, events, locale, agendaCalendarViewListener,
        mAbstractEventRenderer,
        weekends, mWeekendColor, firstDayOfWeek, mEventEmptyVisibility);
  }

  private void init(Calendar minDate, Calendar maxDate, List<CalendarEvent> eventList,
      Locale locale, AgendaCalendarViewListener pickerController,
      AbstractEventRenderer<?> eventRenderer,
      List<Integer> weekends, int weekendColor, int firstDayOfWeek, int emptyEventVisibility) {
    agendaCalendarViewListener = pickerController;

    initCalendarManager(minDate, maxDate, eventList, locale, weekends, weekendColor, firstDayOfWeek,
        emptyEventVisibility);

    // Feed our views with weeks list and events
    mCalendarView.init(mCalendarDayTextColor, mCalendarCurrentDayColor, mCalendarPastDayTextColor,
        firstDayOfWeek);

    // Load agenda events and scroll to current day
    AgendaAdapter agendaAdapter = new AgendaAdapter(mAgendaCurrentDayTextColor, eventRenderer);

    mAgendaView.getAgendaListView().setAdapter(agendaAdapter);
    mAgendaView.getAgendaListView().setOnStickyHeaderChangedListener(this);
    mCalendarView.addWeeksDayClickListener(mAgendaView);
    mCalendarView.addWeekRecyclerViewListener(mAgendaView);
    mAgendaView.setAgendaViewListener(mCalendarView);

    setAgendaCalendarViewFetchListener(mAgendaView);

    animateView();
    // notify that actually everything is loaded
    mAgendaCalendarViewFetchListener.onAgendaCalendarViewEventFetched();
    Log.d(TAG, "CalendarEventTask finished");
  }

  public void enableCalenderView(boolean enable) {
    mAgendaView.enablePlaceholderForCalendar(enable);
    mCalendarView.setVisibility(enable ? VISIBLE : GONE);
    mAgendaView.findViewById(R.id.view_shadow).setVisibility(enable ? VISIBLE : GONE);
  }

  @SuppressLint("RestrictedApi")
  public void enableFloatingIndicator(boolean enable) {
    mFabDirection.setVisibility(enable ? VISIBLE : GONE);
  }

  @Override public void onDayItemClick(IDayItem iDayItem) {
    agendaCalendarViewListener.onDaySelected(iDayItem);
  }

  public List<CalendarEvent> getEvents() {
    return mCalendarManager.getEvents();
  }

  /**
   * Get all events of specific date
   * @param day day of the date in selected month
   * @param month month of date
   * @param year year of date
   * @return List of Events for the date.
   */
  public List<CalendarEvent> getEvents(int day, int month, int year) {
    return mCalendarManager.getEvents(day, month, year, true);
  }

  /**
   * Get events of the specific month
   *
   * @param month month of the event. Remember that the month is start from 0
   * @param year year of the event.
   * @return List of the events.
   */
  public List<CalendarEvent> getEvents(int month, int year) {
    return mCalendarManager.getEvents(month, year);
  }

  public void addEvents(List<CalendarEvent> events) {
    for (int i = 0; i < events.size(); i++) {
      mCalendarManager.addEvent(events.get(i));
    }
    mAgendaCalendarViewFetchListener.refreshAgendaCalendarViewEvent();
  }

  public void addEvent(CalendarEvent event) {
    mCalendarManager.addEvent(event);
    mAgendaCalendarViewFetchListener.refreshAgendaCalendarViewEvent();
  }

  public int deleteEvent(CalendarEvent event) {
    int position = mCalendarManager.deleteEvent(event);
    mAgendaCalendarViewFetchListener.refreshAgendaCalendarViewEvent();
    return position;
  }

  public void updateEvent(CalendarEvent fromEvent) {
    mAgendaCalendarViewFetchListener.refreshAgendaCalendarViewEvent();
  }
}
