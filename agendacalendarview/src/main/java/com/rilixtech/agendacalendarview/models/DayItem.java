package com.rilixtech.agendacalendarview.models;

import android.support.annotation.ColorInt;
import com.rilixtech.agendacalendarview.CalendarManager;
import com.rilixtech.agendacalendarview.utils.DateHelper;
import java.util.Calendar;
import java.util.Date;

/**
 * Day model class.
 */
public class DayItem implements IDayItem {
  private Date mDate;
  private int mValue;
  private int mDayOfTheWeek;
  private boolean mToday;
  private boolean mFirstDayOfTheMonth;
  private boolean mSelected;
  private String mMonth;
  private int mEventTotal;
  private boolean mIsWeekend;
  private int mColor;

  // only for cleanDay
  public DayItem() {}

  private DayItem(DayItem original) {
    mDate = original.getDate();
    mValue = original.getValue();
    mToday = original.isToday();
    mDayOfTheWeek = original.getDayOftheWeek();
    mFirstDayOfTheMonth = original.isFirstDayOfTheMonth();
    mSelected = original.isSelected();
    mMonth = original.getMonth();
    mColor = original.getColor();
    mEventTotal = original.getEventTotal();
    mIsWeekend = original.isWeekend();
  }

  public Date getDate() {
    return mDate;
  }

  public void setDate(Date date) {
    this.mDate = date;
  }

  public int getValue() {
    return mValue;
  }

  public void setValue(int value) {
    this.mValue = value;
  }

  public boolean isToday() {
    return mToday;
  }

  public void setToday(boolean today) {
    this.mToday = today;
  }

  public boolean isSelected() {
    return mSelected;
  }

  public void setSelected(boolean selected) {
    this.mSelected = selected;
  }

  public boolean isFirstDayOfTheMonth() {
    return mFirstDayOfTheMonth;
  }

  public void setFirstDayOfTheMonth(boolean firstDayOfTheMonth) {
    mFirstDayOfTheMonth = firstDayOfTheMonth;
  }

  public String getMonth() {
    return mMonth;
  }

  public void setMonth(String month) {
    this.mMonth = month;
  }

  public int getDayOftheWeek() {
    return mDayOfTheWeek;
  }

  public void setDayOftheWeek(int mDayOftheWeek) {
    this.mDayOfTheWeek = mDayOftheWeek;
  }

  public void setEventTotal(int eventTotal) {
    mEventTotal = eventTotal;
  }

  public int getEventTotal() {
    return mEventTotal;
  }

  public void buildDayItemFromCal(Calendar calendar) {
    mDate = calendar.getTime();
    CalendarManager manager = CalendarManager.getInstance();
    mValue = calendar.get(Calendar.DAY_OF_MONTH);
    mToday = DateHelper.sameDate(calendar, manager.getToday());
    mMonth = manager.getMonthHalfNameFormat().format(mDate);
    if (mValue == 1) mFirstDayOfTheMonth = true;
  }

  @Override @ColorInt public int getColor() {
    return mColor;
  }

  @Override public void setColor(@ColorInt int color) {
    this.mColor = color;
  }

  @Override public String toString() {
    return "DayItem{" + "Date='" + mDate.toString() + ", value=" + mValue + '}';
  }

  @Override public boolean isWeekend() {
    return mIsWeekend;
  }

  @Override public void setWeekend(boolean isWeekend) {
    this.mIsWeekend = isWeekend;
  }

  @Override public IDayItem copy() {
    return new DayItem(this);
  }
}
